package com.mindspore.ide.toolkit.common.events;

import lombok.Getter;
import lombok.Setter;

/**
 * xxx
 *
 * @since 2023-02-07
 */
@Getter
@Setter
public class CommonEvent {
    private String data;
}
