package com.mindspore.ide.toolkit.common.events;

/**
 * xxx
 *
 * @since 2022-11-14
 */
public class SearchSelectMindSpore {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
