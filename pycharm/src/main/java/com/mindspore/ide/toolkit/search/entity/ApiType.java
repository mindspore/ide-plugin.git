package com.mindspore.ide.toolkit.search.entity;

/**
 * xxx
 *
 * @since 2022-12-13
 */
public enum ApiType {
    PyTorch,
    TensorFlow
}
