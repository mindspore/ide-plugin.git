# MindSpore Dev Toolkit README
MindSpore Dev Toolkit是一款面向MindSpore开发者的开发套件。在VSCode的版本上，我们目前支持的功能有智能代码补全。 
## Features
### 智能代码补全
提供基于MindSpore项目的AI代码补全。
无需安装MindSpore环境，也可轻松开发MindSpore。

### API映射搜索

快速搜索MindSpore API映射关系，减少检索时间。

### API映射扫描

扫描文件、文件夹，获取全部PyTorch API，基于MindSpore API映射文档提供的映射关系，给出扫描结果。帮助用户快速迁移项目至MindSpore

